
const express = require('express');
const graphQLHTTP = require('express-graphql');
const port = process.env.PORT || 3000;
const server = express();

const { graphql, buildSchema } = require('graphql');

const schema = buildSchema (`

type Video {
  id: ID,
  title: String,
  duration: Int,
  watched: Boolean
}

type Query {
  video: Video
}

type Schema {
  query: Query
}

`);


const resolvers = {
  video: ()=>({
    id: '1',
    title: 'Stringify',
    duration: 3,
    watched: true,
  }),
};

const query =`
query videoQuery {
  video {
    id, title, duration, watched
  }
}
`;


graphql(schema,query,resolvers)
  .then((result)=>console.log(result))
  .catch((error)=>console.log(error));


server.use('/graphQL', graphQLHTTP({
  schema, 
  graphiql:true,
  //rootValue: resolvers,
}));

server.listen(port, ()=>{
  console.log(`GraphQL server up and running on http://localhost:${port}`);
});