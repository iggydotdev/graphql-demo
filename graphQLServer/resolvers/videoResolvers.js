
const getVideoById = (id,arrayOfItems) => 
  new Promise( (resolve)=>{
    const [res] = arrayOfItems.filter((item)=>{
      return item.id === id;
    });
    resolve(res);
  });

const getVideosFromAuthor = (author,arrayOfItems) => 
  new Promise( (resolve)=>{
    const res = arrayOfItems.filter((item)=>{
      return item.author === author;
    });
    resolve(res);
  });

const getVideosFromAuthorID = (authorID,arrayOfItems) => 
  new Promise( (resolve)=>{
    const res = arrayOfItems.filter((item)=>{
      return item.id === authorID;
    });
    resolve(res);
  });
const getVideos = (arrayOfItems) => 
new Promise ( (resolve)=>{
  const res = arrayOfItems;
  resolve(res);
})
module.exports = {
  getVideos,
  getVideoById,
  getVideosFromAuthor,
  getVideosFromAuthorID
};