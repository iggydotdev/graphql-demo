
const getUsers = (arrayOfItems) => 
  new Promise( (resolve)=>{
    const res = arrayOfItems;
    resolve(res);
  });

const getUser = (id,name,arrayOfItems) => 
  new Promise( (resolve)=>{
    const [res] = arrayOfItems.filter((item)=>{
      if (item.name === name || item.id === id) return item;
    });
    resolve(res);
  });

const addUser = (argid, argname, arrayOfItems) => 
  new Promise( (resolve)=>{
    const res = arrayOfItems.push({id: argid, name: argname});
    resolve(res);
  });

module.exports = {
  getUsers,
  getUser,
  addUser,
};