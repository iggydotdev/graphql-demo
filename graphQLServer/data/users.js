const users = [
  { 
    id: '1', 
    name: 'Keegan Street',
  },
  { 
    id: '2', 
    name: 'Lachlan McDonald',
  },
  { 
    id: '3', 
    name: 'Damian Keeghan',
  },
  { 
    id: '4', 
    name: 'Saxon Cameron',
  },
  { 
    id: '5', 
    name: 'Fortunato Geelhoed',
  },
  { 
    id: '6', 
    name: 'Vy Hoang',
  },
  { 
    id: '7',
    name: 'Jenna Salau',
  },
];

module.exports = users;