const { 
  graphql, 
  buildSchema,
  GraphQLSchema,
  GraphQLNonNull,
  GraphQLBoolean,
  GraphQLID,
  GraphQLInt,
  GraphQLString,
  GraphQLObjectType,
} = require('graphql');

const UserType = require('../schemas/User') ;

const UserMutation = new GraphQLObjectType({
  name: "UserMutations",
  description: "User Mutations",
  fields: {
    addUser: {
      type: UserType,
      args:{
        id: {
          type: new GraphQLNonNull(GraphQLID),
          description: "User Id",
        },
        name: {
          type: new GraphQLNonNull(GraphQLID),
          description: "User name",
        },
      },
      resolve(parentValue,args){
        // do stuff
      }, 
    },
    removeUser:{
      type: UserType,
      args:{
        id: {
          type: new GraphQLNonNull(GraphQLID),
          description: "",
        },
        name: {
          type: new GraphQLNonNull(GraphQLID),
          description: "",
        },
      },
      resolve(parentValue,args){
        //doStuff with mlab!
      },
    },
    modifyUser:{
      type: UserType,
      args:{
        id:{
          type: new GraphQLNonNull(GraphQLID),
          description: "User ID",
        },
        name:{
          type: new GraphQLNonNull(GraphQLID),
          description: "User name",
        },
        company:{
          type: new GraphQLNonNull(GraphQLID),
          description: "User's company",
        },
      },
      resolve(parentValue, args){
        //doStuff
      },
    },
  }
});

module.exports = userMutations;