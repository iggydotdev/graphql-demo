const {graphql, buildSchema} = require('graphql');

const schema = buildSchema(`

type Video {
  id: ID,
  title: String
  duration: Int,
  watched: Boolean 
}
type Query {
  video: Video,
  videos: [Video]
}
type Schema {
  query: Query
}

`);

const resolvers = {
  video: ()=> ({
    id: ()=>'1',
    title: ()=>'Egghead', 
    duration:()=> '180',
    watched: ()=>true,
  }),
  videos: ()=> videos,
};

const videoA = {
  id: '1',
  title: 'Video 1',
  duration: 1,
  watched: false,
};

const videoB = {
  id: '2',
  title: 'Video 2',
  duration: 2,
  watched: true,
};


const videos = [videoA, videoB];



const query = `
query firstQuery {
  video {
    id,
    title,
    duration,
    watched
  }
}
`;

graphql(schema, query, resolvers)
  .then((res)=>console.log(res))
  .catch((error)=>console.log(error));

