const { 
  graphql, 
  buildSchema,
  GraphQLSchema,
  GraphQLNonNull,
  GraphQLBoolean,
  GraphQLID,
  GraphQLInt,
  GraphQLString,
  GraphQLObjectType,
} = require('graphql');

const VideoType = new GraphQLObjectType({
  name: 'videoType',
  description: 'This is a video type',
  fields: {
    id: {
      type: new GraphQLNonNull(GraphQLInt),
      description: 'Id of video',
    },
    title: {
      type: GraphQLString,
      description: 'Title of video',
    }, 
    duration: {
      type: GraphQLInt,
      description: 'Duration of video in minutes',
    },
    author: {
      type: GraphQLString,
      description: 'Video Author'
    },
    url: {
      type: GraphQLString,
      description: 'URL of the video'
    },
    difficulty: {
      type: GraphQLString,
      description: 'Difficulty of the video',
    },
  },
});

module.exports = VideoType;