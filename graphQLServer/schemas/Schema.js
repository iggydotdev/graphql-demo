const { 
  graphql, 
  buildSchema,
  GraphQLSchema,
  GraphQLNonNull,
  GraphQLBoolean,
  GraphQLID,
  GraphQLInt,
  GraphQLString,
  GraphQLObjectType,
  GraphQLList,
} = require('graphql');

const {
  getVideos,
  getVideoById,
  getVideosFromAuthor
} = require("../resolvers/videoResolvers");
const {
  getUsers,
  getUser,
  addUser
} = require('../resolvers/userResolvers');

const videoData = require('../data/videos');
const usersData = require('../data/users');

const videoType = new GraphQLObjectType({
  name: 'videoType',
  description: 'This is a video type',
  fields: ()=>({
    id: {
      type: new GraphQLNonNull(GraphQLInt),
      description: 'Id of video',
    },
    title: {
      type: GraphQLString,
      description: 'Title of video',
    }, 
    duration: {
      type: GraphQLInt,
      description: 'Duration of video in minutes',
    },
    author: {
      type: GraphQLString,
      description: 'Video Author',
    },
    url: {
      type: GraphQLString,
      description: 'URL of the video'
    },
    difficulty: {
      type: GraphQLString,
      description: 'Difficulty of the video',
    },
  })
});

const userType = new GraphQLObjectType({
  name: "User",
  description: "User",
  fields: () => ({
    id: {
      type: GraphQLInt,
      description: "User ID",
    },
    name:{
      type: GraphQLString,
      description: "User Name"
    },
    videos: {
      type: GraphQLList(videoType),
      description: 'User videos',
      resolve: (parentValue, args) => {
        let res = getVideosFromAuthor(parentValue.name, videoData);
        return (res || null)
      },
    }
  })
});


const queryType = new GraphQLObjectType({
  name: 'QueryType',
  description: 'Root query type',
  fields: {
    video: {
      type: videoType,
      description: 'A video type',
      args: {
        id: {
          type: GraphQLID,
          description: "The id of the video",
        },
      },
      resolve:   
        (_, args) => { 
          let res = getVideoById(args.id, videoData);
          console.log('Resolving', res);
          return (res || null) 
        }
    },
    videos: {
      type: new GraphQLList(videoType),
      resolve: 
        () => {
          let res = getVideos(videoData);
          console.log(res);
          return (res || null)
        }
    },
    users: {
      type: new GraphQLList(userType),
      resolve: ()=>{
        let res = getUsers(usersData);
        return (res || null);
      },
    },
    user: {
      type: userType,
      args: {
        id: {
          type: GraphQLID,
          description: 'User ID'
        },
        name: {
          type: GraphQLString,
          description: 'User name',
        },
      },
      resolve: (_,args) => {
        let res = getUser(args.id, args.name, usersData);
        console.log('RES',res);
        return (res || null);
      }
    }
  }
});

const mutation = new GraphQLObjectType({
  name: 'Mutation',
  description: 'mutations for users',
  fields: ()=>({
    addUser: {
      type: userType,
      description: 'Add a user',
      args: {
        id: {
          type: GraphQLNonNull(GraphQLID),
          description: 'new User Id',
        },
        name: {
          type: GraphQLNonNull(GraphQLString),
          description: 'new User name',
        }
      },
      resolve: (parentValue, { id, name }) => {
        let res = addUser(id, name, usersData);
        return res
      }
    }
  })
});

const schema = new GraphQLSchema({
  query: queryType,
  mutation: mutation,
//  subscription,
});

module.exports = schema;
