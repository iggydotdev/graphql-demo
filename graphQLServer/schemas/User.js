const { 
  graphql, 
  buildSchema,
  GraphQLSchema,
  GraphQLNonNull,
  GraphQLBoolean,
  GraphQLID,
  GraphQLInt,
  GraphQLString,
  GraphQLObjectType,
} = require('graphql');

const UserType = new GraphQLObjectType({
  name: "User",
  description: "User Type",
  fields: {
    id: {
      type: GraphQLNonNull(GraphQLID),
      description: 'User ID'
    },
    name:{
      type: GraphQLNonNull(GraphQLString),
      description: 'User full name'
    },
  }
});

module.exports = UserType;