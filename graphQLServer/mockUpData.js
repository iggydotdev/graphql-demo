const videos = [
  { 
    id: '101', 
    title: 'FED101 Intro to Front End Development',
    author: 'Keegan Street',
    url: 'https://www.youtube.com/watch?v=lggGG3tdxUA',
    duration: 46,
    difficulty:'basic'
  },
  { 
    id: '102', 
    title: 'FED102 Intro to Responsive Design',
    author: 'Lachlan McDonald',
    url: 'http://youtube.com/watch?v=RfYs4UEG5X8',
    duration: 46,
    difficulty:'basic'
  },
  { 
    id: '103', 
    title: 'FED103 Intro to Accessibility',
    author: 'Damian Keeghan',
    url: 'https://www.youtube.com/watch?v=i8DOCaNcNJw',
    duration: 56,
    difficulty:'basic'
  },
  { 
    id: '201', 
    title: 'FED201 The Deloitte Digital FED Framework',
    author: 'Damian Keeghan',
    url: 'https://www.youtube.com/watch?v=nXHZ7GTPryA',
    duration: 51,
    difficulty:'intermediate'
  },
  { 
    id: '202', 
    title: 'FED203 GIT for Front End Development',
    author: 'Saxon Cameron',
    url: 'https://www.youtube.com/watch?v=o2WAQTMbuUk',
    duration: 35,
    difficulty:'intermediate'
  },
  { 
    id: '203', 
    title: 'FED203 GIT for Front End Development',
    author: 'Fortunato Geelhoed',
    url: 'https://www.youtube.com/watch?v=IF_Fl5G5e8I',
    duration: 55,
    difficulty:'intermediate'
  },
  { 
    id: '204', 
    title: 'FED204 Cross Browser support and testing',
    author: 'Vy Hoang',
    url: null,
    duration: 0,
    difficulty:'intermediate'
  },
  { 
    id: '205', 
    title: 'FED205 Unit and Functional Testing',
    author: 'Fortunato Geelhoed',
    url: 'https://www.youtube.com/watch?v=ho4qY9S8HPY',
    duration: 55,
    difficulty:'intermediate'
  },
  { 
    id: '301', 
    title: 'FED301 Creating reusable JavaScript modules',
    author: 'Damian Keeghan',
    url: 'https://www.youtube.com/watch?v=wcu2dn2cgn4',
    duration: 51,
    difficulty:'advance',
  },
  { 
    id: '302', 
    title: 'FED302 Intro to AngularJS',
    author: 'Vy Hoang',
    url: 'https://www.youtube.com/watch?v=b1sdMaBEZ3U',
    duration: 54,
    difficulty:'advance',
  },
  { 
    id: '303', 
    title: 'FED303 Intro to React',
    author: 'Jenna Salau',
    url: 'https://www.youtube.com/watch?v=wMVsgRbfbTc',
    duration: 48,
    difficulty:'advance',
  },
];

module.exports = videos;