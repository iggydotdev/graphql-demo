
const express = require('express');
const { graphql } = require('graphql');
const graphQLHTTP = require('express-graphql');
const bodyParser = require('body-parser');
const cors = require('cors');
const port = 4000 || process.env.PORT || 4000;
const server = express();
const schema = require('./schemas/Schema.js');

const graphQLParams = {
  schema: schema, 
  graphiql:true,
  pretty: true,
};

server
  .use(bodyParser.text({ type: '*/*' }))
  .use(bodyParser.json({ type: 'application/graphql'}))
  .use(cors());

//server.use('/graphql',graphQLHTTP(graphQLParams));

server.post('/', (req,res)=>{
  let query = JSON.parse(req.body).query;
  console.log('Query: \n',query);
  graphql(schema,query).then((result)=> { console.log('Result: ', result); res.send(result) });
  console.log('Response sent');
});


server.listen(port, ()=>{
  console.log(`GraphQL server up and running on http://localhost:${port}`);
});
