const mongoose = require('mongoose');
let Schema = mongoose.Schema;

const VideoSchema = new Schema({
  title: String,
  url: String,
  duration: Number,
  author: [{id: Number, name:String}],
  technology: [{id: Number, title:String}]
});

module.exports = videoSchema;