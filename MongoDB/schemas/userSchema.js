const mongoose = require('mongoose');
let Schema = mongoose.Schema;

const userSchema = new Schema({
  id: Schema.ObjectId,
  name: String,
  companyid: Schema.ObjectId,
});

module.exports = userSchema;
