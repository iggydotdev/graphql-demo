import express from 'express';
import path from 'path';
import bodyParser from 'body-parser';
import { renderToString } from 'react-dom/server'; 
import React from 'react';
// import App from '../containers/App';

import fetch from 'node-fetch';
import { ApolloProvider } from 'react-apollo';
import { ApolloClient } from 'apollo-client';
import { createHttpLink } from 'apollo-link-http';
import { InMemoryCache } from 'apollo-cache-inmemory';
import { StaticRouter as Router } from 'react-router-dom';
import Main from '../containers/Main';

const port = process.env.port || 3000;
const app = express();

app.use(bodyParser.text({ type: '*/*' }));
//app.use(express.static(path.resolve (__dirname, './client')));

app.use(express.static(`${__dirname}/../client`));

const httpLink = createHttpLink({
  uri: 'http://localhost:4000/',
  fetch: fetch,
});
const client = new ApolloClient({
  link: httpLink,
  cache: new InMemoryCache({
    addTypename: false
  })
});

app.get('/*', (req,res) => {
  const context={};
  const jsx = (
    // <App req={request} />
    <ApolloProvider client={client}>
      <Router context={context} location={req.url}>
        <Main request={req}/>
      </Router>  
    </ApolloProvider>
  );
  const reactDom = renderToString(jsx);
  //res.writeHead(200, {'Content-Type': 'text/html'}); 
  res.send( htmlTemplate(reactDom) )
});

app.listen(port,()=>{
  console.log(`Server ready at localhost:${port}`)
});

let htmlTemplate = ( container ) => `
  <!DOCTYPE html>
    <html>
    <head>
      <meta charset="utf-8" />
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <title>GraphQL - Server Side</title>
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <style>
        html, body {
          margin:0;
          padding: 0;
        }
        #root {
          width:100%;
          height:auto;
        }
      </style>
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    </head>

    <body>
      <div id='root'>${container}</div>
      <script src="client.js"></script>
      <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
      <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
    </body>
    </html>`;