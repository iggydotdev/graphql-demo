import Home from '../containers/Home/Home';
import Users from '../containers/Users/Users';
import User from '../containers/User/User';
import Videos from '../containers/Videos/Videos';
import Video from '../containers/Video/Video';

export { 
  Home,
  Users,
  User,
  Videos,
  Video,
};