import React from 'react';
import ReactDOM from 'react-dom';
//import App from '../containers/App';
import { BrowserRouter as Router } from 'react-router-dom';
import Main from '../containers/Main';

import fetch from 'node-fetch';
import { ApolloProvider } from 'react-apollo';
import { ApolloClient } from 'apollo-client';
import { createHttpLink } from 'apollo-link-http';
import { InMemoryCache } from 'apollo-cache-inmemory';

const httpLink = createHttpLink({
  uri: 'http://localhost:4000/',
  fetch: fetch,
});
const client = new ApolloClient({
  link: httpLink,
  cache: new InMemoryCache({
    addTypename: false
  })
});

ReactDOM.hydrate(
  <ApolloProvider client={client}>
    <Router>
      <Main/>
    </Router>  
  </ApolloProvider>, 
  document.getElementById('root')
);