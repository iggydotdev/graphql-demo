import React from 'react';
import { Link } from 'react-router-dom';

const GlobalFooter = (props) => 
  <footer className='row' style={{marginBottom: '40px'}}>
    <div className='col'/>
    <Link to='/' className='col' style={{textAlign:'center'}}>
      <span>FEDeration</span>
    </Link>
    <div className='col'/>
  </footer>;

export default GlobalFooter;