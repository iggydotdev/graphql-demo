import React from 'react';
import User from '../User/User';

const UsersList = (props) => { 
  const listOfUsers = props.data.map((item) => {
    return (<User key={item.id} data={item}/>);
  });
  return (
    <ul style={{listStyle:'none'}}>
      {listOfUsers}
    </ul>
  );
};

export default UsersList;