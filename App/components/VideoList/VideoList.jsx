import React from 'react';
import Video from '../Video/Video';

const VideoList = (props) => { 
  console.log(props.data);
  const listOfVideos = props.data.map((item) => {
    return (<Video key={`${item.id}`} data={item}/>);
  });
  return (
    <ul style={{listStyle:'none'}}>
      {listOfVideos}
    </ul>
  );
};

export default VideoList;