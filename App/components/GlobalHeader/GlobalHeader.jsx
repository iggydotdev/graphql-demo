import React from 'react';
import { NavLink } from 'react-router-dom';

class GlobalHeader extends React.Component{
  constructor(props) {
    super(props)
    
  }

  render(){
    return (
      <header className=''>
        <nav className="navbar navbar-expand-lg navbar-light bg-light" >
          <NavLink className="navbar-brand" to="/">GraphQL Demo</NavLink>
          <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span className="navbar-toggler-icon"></span>
          </button>
          <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav mr-auto">
            <li className="nav-item" >
              <NavLink 
                to='/users'
                className='nav-link' 
                activeStyle={
                  {
                    color: '#86f200',
                    textDecoration: 'none',
                  }
                }
              >
                Users
              </NavLink>
            </li>
            <li className='nav-item'>
              <NavLink 
                to='/videos' 
                className='nav-link'
                activeStyle={
                  {
                    color: '#86f200',
                    textDecoration: 'none',
                  }
                }
              >
                Videos
              </NavLink>
            </li>
          </ul>
          </div>
        </nav>
      </header>
    );
  }
}

export default GlobalHeader;
