import React from 'react';
import UserStyles from './UserStyles';
import { Link } from 'react-router-dom';

const UserItem = (props) => {

  let videoList = (props.data.videos!==undefined && props.data.videos!==null)? 
    props.data.videos.map((item)=>{
      return <li key={`user-video-${item.id}`}><a href={item.url}>{item.title}</a></li>;
    }): null;
  return (
    <React.Fragment>  
    <li style={ UserStyles.li }>
      <Link style={ UserStyles.a } 
        to={`/users/${props.data.id}`} 
      >
        {props.data.name}
      </Link>
    </li>
    { (videoList!==null) ? 
      (<ul style={{textAlign: 'center', listStyle: 'none'}}>{videoList}</ul>) : null   
    }
    </React.Fragment>
  );

};

export default UserItem;