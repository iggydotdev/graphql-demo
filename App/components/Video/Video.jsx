import React from 'react';
import { Link } from 'react-router-dom';

const VideoItem = (props) => {
  return (
    <React.Fragment>  
      <li key={`video-${props.data.id}`} style={{ margin: '20px auto', textAlign:'center'}}>
        <Link to={`/videos/${props.data.id}`} style={{textDecoration:'none'}}>
          {`${props.data.title}`}
        </Link>
      </li>
    </React.Fragment>
  );

};

export default VideoItem;