import * as Containers from "../config/containers";

const fetchContainer = (Containers, container) => {
  if (Containers.hasOwnProperty(container)) {
    return Containers[container];
  }
  return Containers["Home"];
};

export default {
  routes: [
    {
      path: '/',
      component: fetchContainer(Containers, 'Home'),
      exact: true,
    },
    {
      path: '/users',
      component: fetchContainer(Containers, 'Users'),
      exact: true,
    },
    {
      path: '/users/:userId',
      component: fetchContainer(Containers, 'User'),
      exact: true, 
    },
    {
      path: '/videos',
      component: fetchContainer(Containers, 'Videos'),
      exact: true,
    },
    {
      path: '/videos/:videoId',
      component: fetchContainer(Containers, 'Video'),
      exact: true,
    }
  ]
};