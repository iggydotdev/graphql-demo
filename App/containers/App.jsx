import React from 'react'; 
import {ApolloProvider} from 'react-apollo';
import { StaticRouter as Router} from 'react-router-dom';

import fetch from 'node-fetch';
import { ApolloClient } from 'apollo-client';
import { createHttpLink } from 'apollo-link-http';
import { InMemoryCache } from 'apollo-cache-inmemory';
import Main from '../containers/Main';



class App extends React.Component{
  constructor (props) {
    super(props);
  }
  
  render(){

    const context={};
    
    console.log('Request received: \n', this.props.req.url);

    const httpLink = createHttpLink({
      uri: 'http://localhost:4000/',
      fetch: fetch,
    });
    const client = new ApolloClient({
      link: httpLink,
      cache: new InMemoryCache({
        addTypename: false
      })
    });
    client.resetStore();
    return (
      <ApolloProvider client={client}>
        <Router context={context} location={this.props.req.url}>
          <Main/>
        </Router>  
      </ApolloProvider>
    );
  }
}

export default App;


