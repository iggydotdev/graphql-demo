import React from 'react';
import { Query } from 'react-apollo';
import gql from 'graphql-tag'; 

import UsersList from '../../components/UsersList/UsersList';

// import left from '../img/pexels-photo-774909.jpeg';
// import right from '../img/pexels-photo-220453.jpeg';


const MyComponent = (props) => {
  const messageStyle = { padding: '20px 40px', };
  return (
    <div style={Object.assign(messageStyle, {})}>
      <p>Here it is: </p><pre>{JSON.stringify(props.fetchedData,null,2)}</pre>
      <UsersList data={props.fetchedData.users}/>
    </div>
    
  );
};

class Users extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
 
    }
  }

  render() {
    
    let queryUsers = `{ users { id, name }}`;
    let queryString = queryUsers;
    
    let mySuperCoolQuery = gql`
      ${queryString}
    `;
 
    const messageStyle = { padding: '20px 40px' };

    return (
      <div style={{ width: '85%', height: 'auto', margin: '60px auto', }}>
        <div style={{ textAlign: 'center' }}>
          <div style={messageStyle}>
            <p>Hello Apollo!</p>
            <p>Where is the data I asked for?</p>
            <p>( {queryString} )</p>
          </div>
        </div>
        <div style={{ textAlign: 'center', marginTop: '20px' }}>
          <div style={{}}>
            <Query query={mySuperCoolQuery}>
              {({ loading, error, data }) => {
                if (loading) {
                  return (
                    <div style={messageStyle}>
                      It's coming! Give me a moment to check the GraphQL server…
                    </div>
                  );
                }
                if (error) {
                  return (
                    <div style={messageStyle}>
                      I couldn't get anything but a big error! Something went
                      wrong!
                    </div>
                  );
                }
                return <MyComponent fetchedData={data} />;
              }}
            </Query>
          </div>
        </div>
      </div>
    );
  }
}

export default Users;

