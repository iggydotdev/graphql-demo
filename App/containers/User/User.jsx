import React from 'react';
import { Query } from 'react-apollo';
import gql from 'graphql-tag'; 
import UserItem from '../../components/User/User';

const User = (props) => {
  let queryUsers = `{ user(id: ${props.match.params.userId}) {id, name, videos { url, title } }}`;
    let queryString = queryUsers;
    
    let mySuperCoolQuery = gql`
      ${queryString}
    `;
    const messageStyle = { padding: '20px 40px' };
  return (
    <Query query={mySuperCoolQuery} >
      {({ loading, error, data }) => {
        if (loading) {
          return (
            <div style={messageStyle}>
              It's coming! Give me a moment to check the GraphQL server…
            </div>
          );
        }
        if (error) {
          return (
            <div style={messageStyle}>
              I couldn't get anything but a big error! Something went
              wrong!
            </div>
          );
        }
        return <UserItem data={data.user}/>
      }}
    </Query>
  );
}

export default User;