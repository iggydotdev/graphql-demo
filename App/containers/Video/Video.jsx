import React from 'react';
import { Query } from 'react-apollo';
import gql from 'graphql-tag'; 
import VideoItem from '../../components/Video/Video';

const Video = (props) => {
  let queryVideo = `{ video(id: ${props.match.params.videoId}) { id, author, title, url }}`;
    let queryString = queryVideo;
    
    let mySuperCoolQuery = gql`
      ${queryString}
    `;
    const messageStyle = { padding: '20px 40px' };
  return (
    <React.Fragment>
      <Query query={mySuperCoolQuery} >
        {({ loading, error, data }) => {
          if (loading) {
            return (
              <div style={messageStyle}>
                It's coming! Give me a moment to check the GraphQL server…
              </div>
            );
          }
          if (error) {
            return (
              <div style={messageStyle}>
                I couldn't get anything but a big error! Something went
                wrong!
              </div>
            );
          }
          return <VideoItem data={data.video} match={props.match}/>
        }}
      </Query>
    </React.Fragment>
  );
}

export default Video;