import React from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';
import GlobalHeader from '../components/GlobalHeader/GlobalHeader';
import GlobalFooter from '../components/GlobalFooter/GlobalFooter';
import routesBank from '../routes/routes';

const Main = (props) => {
   const routeList = routesBank.routes.map((route, index)=>
    <Route 
      key={`route${index}`}
      path={route.path} 
      component={route.component}
      exact={route.exact}
    />
   );
    console.log(props);
    //props.client.resetStore();
    return (
      <React.Fragment>
        <GlobalHeader/>
        <Switch>
          {routeList}
        </Switch>
        <GlobalFooter/>
      </React.Fragment>
    );

}

export default Main;